package com.teamcodeflux.android.training.app.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.teamcodeflux.android.training.app.R;
import com.teamcodeflux.android.training.app.event.EntryEvent;
import com.teamcodeflux.android.training.app.event.EventBusProvider;
import com.teamcodeflux.android.training.app.event.EventBusProvider_;
import com.teamcodeflux.android.training.app.model.Entry;
import com.teamcodeflux.android.training.app.service.WebService_;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.parceler.Parcels;

@EActivity(R.layout.activity_create_entry)
public class CreateEntryActivity extends Activity {

    @ViewById
    TextView title;

    @ViewById
    TextView body;

    @Bean(EventBusProvider.class)
    Bus bus;

    @Override
    protected void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    @Click(R.id.create_entry)
    void createEntry() {
        Entry entry = new Entry();
        entry.setUserId(1L);
        entry.setTitle(title.getText().toString());
        entry.setBody(body.getText().toString());
        Parcelable parcelableEntry = Parcels.wrap(entry);
        WebService_.intent(this).postEntry(parcelableEntry).start();
    }

    @Subscribe
    public void onEntryEvent(EntryEvent event) {
        Toast.makeText(this, "Entry created", Toast.LENGTH_SHORT).show();
    }
}
package com.teamcodeflux.android.training.app.activity;

import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import com.teamcodeflux.android.training.app.R;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringArrayRes;


@EActivity(R.layout.activity_display_list)
public class DisplayListActivity extends Activity {

    @ViewById
    ListView listView;

    @StringArrayRes(R.array.cities)
    String[] cities;

    @AfterViews
    void afterViews() {
        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                cities);
        listView.setAdapter(stringArrayAdapter);
    }

    @ItemClick(R.id.list_view)
    void itemClicked(int position) {
        String selected = (String) listView.getAdapter().getItem(position);
        Toast toast = Toast.makeText(this, selected, Toast.LENGTH_SHORT);
        toast.show();
    }
}
package com.teamcodeflux.android.training.app.activity;

import android.app.Activity;
import android.widget.TextView;
import com.teamcodeflux.android.training.app.R;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_display_message)
public class DisplayMessageActivity extends Activity {

    @ViewById(R.id.message)
    TextView textView;

    @Extra(MainActivity.EXTRA_MESSAGE)
    String message;

    @AfterViews
    void afterViews() {
        textView.setText(message);
    }
}
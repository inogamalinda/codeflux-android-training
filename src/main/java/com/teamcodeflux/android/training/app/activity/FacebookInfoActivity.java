package com.teamcodeflux.android.training.app.activity;

import android.app.Activity;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import com.teamcodeflux.android.training.app.R;
import org.androidannotations.annotations.*;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

@EActivity(R.layout.activity_facebook_info)
public class FacebookInfoActivity extends Activity {

    private static final String URL = "http://graph.facebook.com/{query}";

    @ViewById
    EditText findPerson;

    @ViewById
    TextView username;

    @ViewById
    TextView profileId;

    @ViewById
    TextView firstName;

    @ViewById
    TextView lastName;

    JSONObject jsonResult;

    @Click(R.id.get_fb)
    void getFb() {
        processFacebook();
    }

    @Background
    void processFacebook() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        String result = restTemplate.getForObject(URL, String.class, findPerson.getText().toString());
        try {
            jsonResult = new JSONObject(result);
        } catch (JSONException e) {
            Log.e("FacebookInfo", "Mapping JSONObject", e);
        }
        updateTextViews();
    }

    @UiThread
    void updateTextViews() {
        try {
            username.setText(jsonResult.getString("username"));
            profileId.setText(jsonResult.getString("id"));
            firstName.setText(jsonResult.getString("first_name"));
            lastName.setText(jsonResult.getString("last_name"));
        } catch (JSONException e) {
            Log.e("FacebookInfo", "Updating TextViews", e);
        }
    }
}
package com.teamcodeflux.android.training.app.activity;

import android.app.Activity;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import com.teamcodeflux.android.training.app.R;
import com.teamcodeflux.android.training.app.adapter.MainDrawerListAdapter;
import org.androidannotations.annotations.*;

@EActivity(R.layout.main)
@OptionsMenu(R.menu.main_activity_menu)
public class MainActivity extends Activity {
    public static final String EXTRA_MESSAGE = "com.teamcodeflux.android.training.app.extra_message";

    @ViewById(R.id.edit_message)
    EditText editText;

    @ViewById(R.id.left_drawer_list)
    ListView drawerList;

    @Bean
    MainDrawerListAdapter adapter;

    @AfterViews
    void afterViews() {
        drawerList.setAdapter(adapter);
    }

    @Click(R.id.send)
    void sendMessage() {
        String message = editText.getText().toString();

        DisplayMessageActivity_.intent(this).message(message).start();
    }

    @OptionsItem(R.id.action_list)
    void showList() {
        DisplayListActivity_.intent(this).start();
    }

    @OptionsItem(R.id.action_fb)
    void showFacebook() {
        FacebookInfoActivity_.intent(this).start();
    }

    @OptionsItem(R.id.action_rest)
    void showRestClient() {
        RestClientActivity_.intent(this).start();
    }

    @OptionsItem(R.id.action_post)
    void createEntry() {
        CreateEntryActivity_.intent(this).start();
    }

    @OptionsItem(R.id.action_maps)
    void showMap() {
        GoogleMapsActivity_.intent(this).start();
    }

    @ItemClick(R.id.left_drawer_list)
    void drawerItemClicked(int position) {
        String selected = adapter.getItem(position);
        Toast toast = Toast.makeText(this, selected, Toast.LENGTH_SHORT);
        toast.show();
    }
}

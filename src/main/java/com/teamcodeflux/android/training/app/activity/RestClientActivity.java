package com.teamcodeflux.android.training.app.activity;

import android.app.Activity;
import android.widget.ListView;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.teamcodeflux.android.training.app.R;
import com.teamcodeflux.android.training.app.adapter.RestClientListAdapter;
import com.teamcodeflux.android.training.app.event.EntryEvent;
import com.teamcodeflux.android.training.app.event.EventBusProvider;
import com.teamcodeflux.android.training.app.event.EventResult;
import com.teamcodeflux.android.training.app.model.Entry;
import com.teamcodeflux.android.training.app.model.data.DatabaseManager;
import com.teamcodeflux.android.training.app.service.WebService_;
import org.androidannotations.annotations.*;

import java.util.ArrayList;
import java.util.List;

@EActivity(R.layout.activity_rest_client)
public class RestClientActivity extends Activity {

    @ViewById(R.id.list_entries)
    ListView entriesListView;

    @Bean
    RestClientListAdapter adapter;

    @Bean(EventBusProvider.class)
    Bus bus;

    @Bean
    DatabaseManager dbManager;

    List<Entry> entries = new ArrayList<Entry>();

    @AfterViews
    void afterViews() {
        fillAdapterEntries();
        entriesListView.setAdapter(adapter);
        WebService_.intent(this).getEntries().start();
    }

    private void fillAdapterEntries() {
        entries = dbManager.getEntries();
        adapter.setEntries(entries);
    }

    @UiThread
    void updateListView() {
        fillAdapterEntries();
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    @Subscribe
    public void onEventResult(EventResult result) {
        fillAdapterEntries();
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbManager.destroy();
    }
}
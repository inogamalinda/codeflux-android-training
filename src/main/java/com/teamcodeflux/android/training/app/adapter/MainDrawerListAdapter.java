package com.teamcodeflux.android.training.app.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.teamcodeflux.android.training.app.R;
import com.teamcodeflux.android.training.app.viewgroup.MainDrawerItemViewGroup;
import com.teamcodeflux.android.training.app.viewgroup.MainDrawerItemViewGroup_;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.res.StringArrayRes;

@EBean
public class MainDrawerListAdapter extends BaseAdapter {

    @RootContext
    Context context;

    @StringArrayRes(R.array.main_drawer_items)
    String[] items;

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public String getItem(int i) {
        return items[i];
    }

    @Override
    public long getItemId(int i) {
        return (long) i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        MainDrawerItemViewGroup itemViewGroup;

        if (view == null) {
            itemViewGroup = MainDrawerItemViewGroup_.build(context);
        } else {
            itemViewGroup = (MainDrawerItemViewGroup) view;
        }

        itemViewGroup.bind(getItem(i));

        return itemViewGroup;
    }
}

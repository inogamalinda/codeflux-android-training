package com.teamcodeflux.android.training.app.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.teamcodeflux.android.training.app.model.Entry;
import com.teamcodeflux.android.training.app.viewgroup.RestClientItemViewGroup;
import com.teamcodeflux.android.training.app.viewgroup.RestClientItemViewGroup_;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;
import java.util.List;

@EBean
public class RestClientListAdapter extends BaseAdapter {

    @RootContext
    Context context;

    List<Entry> entries = new ArrayList<Entry>();

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }

    @Override
    public int getCount() {
        return entries.size();
    }

    @Override
    public Entry getItem(int i) {
        return entries.get(i);
    }

    @Override
    public long getItemId(int i) {
        return entries.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        RestClientItemViewGroup itemViewGroup;

        if (view == null) {
            itemViewGroup = RestClientItemViewGroup_.build(context);
        } else {
            itemViewGroup = (RestClientItemViewGroup) view;
        }

        itemViewGroup.bind(getItem(i));

        return itemViewGroup;
    }
}

package com.teamcodeflux.android.training.app.event;

import com.teamcodeflux.android.training.app.model.Entry;

public class EntryEvent {
    Entry entry;

    public EntryEvent(Entry entry) {
        this.entry = entry;
    }
    public Entry getEntry() {
        return entry;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }
}

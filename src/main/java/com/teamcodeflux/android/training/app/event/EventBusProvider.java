package com.teamcodeflux.android.training.app.event;

import com.squareup.otto.Bus;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.UiThread;

@EBean(scope = EBean.Scope.Singleton)
public class EventBusProvider extends Bus {

    @Override
    public void post(final Object event) {
        doInMainThread(event);
    }

    @UiThread
    void doInMainThread(final Object event) {
        EventBusProvider.super.post(event);
    }
}

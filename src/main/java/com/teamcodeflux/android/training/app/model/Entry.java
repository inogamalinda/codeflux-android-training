package com.teamcodeflux.android.training.app.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.parceler.Parcel;

@Parcel
@DatabaseTable(tableName = "entries")
public class Entry {

    @DatabaseField
    Long userId;

    @DatabaseField(id = true)
    Long id;

    @DatabaseField
    String title;

    @DatabaseField
    String body;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}

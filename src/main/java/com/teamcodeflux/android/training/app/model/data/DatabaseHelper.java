package com.teamcodeflux.android.training.app.model.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.teamcodeflux.android.training.app.model.Entry;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.OrmLiteDao;

import java.sql.SQLException;

@EBean
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DB_NAME = "com.gamalinda.android.poc.testapp.db";
    private static final int DB_VERSION = 1;

    @OrmLiteDao(helper = DatabaseHelper.class, model = Entry.class)
    Dao<Entry, Long> entryDao;

    ConnectionSource connectionSource;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        this.connectionSource = connectionSource;

        try {
            createTables();
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getSimpleName(), "Unable to create tables", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i1) {

    }

    void createTables() throws SQLException {
        TableUtils.createTable(connectionSource, Entry.class);
    }

    @Override
    public void close() {
        super.close();

        entryDao = null;
    }
}

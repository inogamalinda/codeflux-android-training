package com.teamcodeflux.android.training.app.model.data;

import android.content.Context;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.teamcodeflux.android.training.app.model.Entry;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@EBean
public class DatabaseManager {

    @RootContext
    Context context;

    @Bean
    DatabaseHelper dbHelper;

    public void destroy() {
        if (dbHelper != null) {
            OpenHelperManager.releaseHelper();
            dbHelper = null;
        }
    }

    public DatabaseHelper getHelper() {
        if (dbHelper == null) {
            dbHelper = OpenHelperManager.getHelper(context, DatabaseHelper_.class);
        }
        return dbHelper;
    }

    public void createEntry(Entry entry) {
        try {
            getHelper().entryDao.create(entry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createEntries(List<Entry> entries) {
        for (Entry entry : entries) {
            createEntry(entry);
        }
    }

    public void getEntry(Long id) {
        try {
            getHelper().entryDao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Entry> getEntries() {
        List<Entry> entries = new ArrayList<Entry>();
        try {
            entries = getHelper().entryDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return entries;
    }
}
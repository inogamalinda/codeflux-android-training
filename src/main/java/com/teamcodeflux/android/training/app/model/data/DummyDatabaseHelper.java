package com.teamcodeflux.android.training.app.model.data;

import com.teamcodeflux.android.training.app.model.Entry;
import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.List;

@EBean(scope = EBean.Scope.Singleton)
public class DummyDatabaseHelper {
    public static List<Entry> entries = new ArrayList<Entry>();

    public static List<Entry> getEntries() {
        return entries;
    }

    public static void setEntries(List<Entry> entries) {
        DummyDatabaseHelper.entries = entries;
    }
}
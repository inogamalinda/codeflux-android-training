package com.teamcodeflux.android.training.app.service;

import com.teamcodeflux.android.training.app.model.Entry;
import org.androidannotations.annotations.rest.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Rest(rootUrl = "http://jsonplaceholder.typicode.com", converters = {MappingJackson2HttpMessageConverter.class})
public interface RestMethod {

    @Get("/posts")
    List<Entry> getEntries();

    @Get("/posts/{id}")
    Entry getEntry(Long id);

    @Post("/posts")
    Entry addAndReturnEntry(Entry entry);

    @Put("/posts")
    Entry updateEntry(Entry entry);

    @Delete("/posts/{id}")
    Entry deleteEntry(Long id);

    RestTemplate getRestTemplate();

    void setRestTemplate(RestTemplate restTemplate);
}

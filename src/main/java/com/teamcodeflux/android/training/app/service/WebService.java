package com.teamcodeflux.android.training.app.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Parcelable;
import com.squareup.otto.Bus;
import com.teamcodeflux.android.training.app.event.EntryEvent;
import com.teamcodeflux.android.training.app.event.EventBusProvider;
import com.teamcodeflux.android.training.app.event.EventResult;
import com.teamcodeflux.android.training.app.model.Entry;
import com.teamcodeflux.android.training.app.model.data.DatabaseManager;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EIntentService;
import org.androidannotations.annotations.ServiceAction;
import org.androidannotations.annotations.rest.RestService;
import org.parceler.Parcels;

@EIntentService
public class WebService extends IntentService {

    @RestService
    RestMethod restMethod;

    @Bean(EventBusProvider.class)
    Bus bus;

    @Bean
    DatabaseManager dbManager;

    public WebService() {
        super(WebService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dbManager.destroy();
    }

    @ServiceAction
    void getEntries() {
        dbManager.createEntries(restMethod.getEntries());
        bus.post(new EventResult());
    }

    @ServiceAction
    void postEntry(Parcelable parcelableEntry) {
        Entry entry = Parcels.unwrap(parcelableEntry);
        Entry newEntry = restMethod.addAndReturnEntry(entry);
        dbManager.createEntry(newEntry);
        bus.post(new EntryEvent(newEntry));
    }
}

package com.teamcodeflux.android.training.app.viewgroup;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.teamcodeflux.android.training.app.R;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.drawer_item_main)
public class MainDrawerItemViewGroup extends LinearLayout {

    @ViewById(R.id.item_name)
    TextView itemName;

    public MainDrawerItemViewGroup(Context context) {
        super(context);
    }

    public void bind(String name) {
        itemName.setText(name);
    }
}

package com.teamcodeflux.android.training.app.viewgroup;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.teamcodeflux.android.training.app.R;
import com.teamcodeflux.android.training.app.model.Entry;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.list_item_rest_client)
public class RestClientItemViewGroup extends LinearLayout {

    @ViewById(R.id.entry_id)
    TextView entryId;

    @ViewById(R.id.entry_title)
    TextView entryTitle;

    @ViewById(R.id.entry_body)
    TextView entryBody;


    public RestClientItemViewGroup(Context context) {
        super(context);
    }

    public void bind(Entry entry) {
        entryId.setText(entry.getId().toString());
        entryTitle.setText(entry.getTitle());
        entryBody.setText(entry.getBody());
    }
}
